#pragma once
#if !defined(MESHMOD_MESHMOD_H)
#define MESHMOD_MESHMOD_H

#include "core/core.h"
#include "math/vector_math.h"
#include "meshmod/types.h"
#include "meshmod/indextypes.h"

#endif // MESHMOD_MESHMOD_H